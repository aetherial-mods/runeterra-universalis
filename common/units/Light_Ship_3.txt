type = light_ship

hull_size = 20
base_cannons = 20
blockade = 10

sail_speed = 10

trade_power = 3.0
sailors = 100

sprite_level = 3