#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 150 255 150 }

revolutionary_colors = { 8 1 8 }

historical_idea_groups = {
	administrative_conduct
	tech_lv1_infantry_ideas
	magic_lv1_magma_school
	tech_lv2_cavalry_ideas
	tech_lv3_artillery_ideas
	magic_lv3_spirit_school
	magic_lv3_celestial_school
	magic_lv3_blood_school
}

historical_units = {
	Isles_Artillery_1
	Isles_Artillery_2
	Isles_Artillery_3
	Isles_Artillery_4
	Isles_Artillery_5
	Isles_Cavalry_1
	Isles_Cavalry_2
	Isles_Cavalry_3
	Isles_Cavalry_4
	Isles_Cavalry_5
	Isles_Infantry_1
	Isles_Infantry_2
	Isles_Infantry_3
	Isles_Infantry_4
	Isles_Infantry_5
}

monarch_names = {
	"Manousos #0" = 100
	"Giannis #0" = 100
	"Thalis #0" = 100
	"Paraskevas #0" = 100
	"Andronikos #0" = 100
	"Antonios #0" = 100
	"Chysanthos #0" = 100
	"Epameinondas #0" = 100
	"Markos #0" = 100
	"Babis #0" = 100
	"Monakos #0" = 100
	"Tatakis #0" = 100
	"Mutatos #0" = 100
	"Biras #0" = 100
	"Siskellis #0" = 100
	"Antonopoulos #0" = 100
	"Matellis #0" = 100
	"Valliadis #0" = 100
	"Fotoulis #0" = 100
	"Perriades #0" = 100
	"Titos #0" = 100
	"Serafeim #0" = 100
	"Nektarios #0" = 100
	"Tolis #0" = 100
	"Alexis #0" = 100
	"Efstathios #0" = 100
	"Leonidas #0" = 100
	"Aristarchos #0" = 100
	"Iasonas #0" = 100
	"Argiris #0" = 100
	
	"Lambrini #0" = 100
	"Maya #0" = 100
	"Marieta #0" = 100
	"Georgia #0" = 100
	"Kriti #0" = 100
	"Iris #0" = 100
	"Ariadni #0" = 100
	"Artemis #0" = 100
	"Emmanouella #0" = 100
	"Evi #0" = 100
	"Chronili #0" = 100
	"Gallopoulou #0" = 100
	"Spera #0" = 100
	"Comea #0" = 100
	"Zenalli #0" = 100
	"Spitereli #0" = 100
	"Stamatopoulou #0" = 100
	"Karahaliea #0" = 100
	"Teresili #0" = 100
	"Totea #0" = 100
	"Efrosyni #0" = 100
	"Filia #0" = 100
	"Kalliopi #0" = 100
	"Efthimia #0" = 100
	"Evlampia #0" = 100
	"Avgi #0" = 100
	"Evdoksia #0" = 100
	"Irene #0" = 100
	"Palaioli #0" = 100
	"Agapi #0" = 100
}

leader_names = {
	Manousos
	Giannis
	Thalis
	Paraskevas
	Andronikos
	Antonios
	Chysanthos
	Epameinondas
	Markos
	Babis
	Monakos
	Tatakis
	Mutatos
	Biras
	Siskellis
	Antonopoulos
	Matellis
	Valliadis
	Fotoulis
	Perriades
	Titos
	Serafeim
	Nektarios
	Tolis
	Alexis
	Efstathios
	Leonidas
	Aristarchos
	Iasonas
	Argiris
}

ship_names = {
	Zanili Karalouli Malelli Palaiotzi Dimide Zenatou Frangiade Pardili Teresiadi Stavroti
}

army_names = {
	"Armee von $PROVINCE$"
}

fleet_names = {
	"Fleet of $PROVINCE$"
}