military_hegemon = {
	allow = {
		is_great_power = yes
		army_size = 250
		NOT = { any_other_great_power = { army_size = root } }
		NOT = { has_country_modifier = lost_hegemony }
	}
	
	base = {
		free_mil_policy = 1
	    free_leader_pool = 1
		war_exhaustion = -0.1
		global_spy_defence = 0.25
		province_warscore_cost = -0.1
	}
	
	scale = {
		movement_speed = 0.1
		land_attrition = -0.2
		global_unrest = -3
	}
	
	max = {
		siege_ability = 0.2
	}
}