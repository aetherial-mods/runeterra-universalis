monopoly_aspect = {
	sprite = "GFX_CA_monopoly_aspect_icon"
	cost = 50
	trigger = {
		NOT = {
			mercantilism = 100
		}
	}
	effect = {
		add_mercantilism = 1
	}
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			trade_income_percentage = 0.4
		}
	}
}

government_aspect = {
	sprite = "GFX_CA_government_aspect_icon"
	cost = 100
	trigger = {
		NOT = {
			treasury = 100000
		}
	}
	effect = {
		add_legitimacy = 10
		add_republican_tradition = 5
		add_devotion = 10
		add_horde_unity = 10
		add_meritocracy = 10
	}
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			NOT = { legitimacy = 75 }
		}
		modifier = {
			factor = 2
			NOT = { legitimacy = 50 }
		}
		modifier = {
			factor = 2
			NOT = { legitimacy = 25 }
		}
	}
}

stability_aspect = {
	sprite = "GFX_CA_stability_aspect_icon"
	cost = 200
	trigger = {
		NOT = {
			stability = 3
		}
	}
	effect = {
		add_stability = 1
	}
	ai_will_do = {
		factor = 3
		modifier = {
			factor = 0.5
			stability = 0
		}
		modifier = {
			factor = 0.5
			stability = 1
		}
		modifier = {
			factor = 0.5
			stability = 2
		}
	}
}