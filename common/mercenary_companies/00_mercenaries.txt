free_company = {
	regiments_per_development = 0.04
	cavalry_weight = 0.25
	cavalry_cap = 4
	artillery_weight = 0.25
	sprites = { westerngfx_sprite_pack }
	trigger = {
		NOT = {
			technology_group = void
			religion = the_watchers
		}
	}
}

grand_company = {
	regiments_per_development = 0.08
	cavalry_weight = 0.25
	cavalry_cap = 8
	artillery_weight = 0.25
	sprites = { westerngfx_sprite_pack }
	trigger = {
		NOT = {
			technology_group = void
			religion = the_watchers
		}
	}
}

void_support = {
	regiments_per_development = 0.01
	cavalry_weight = 1
	sprites = { westerngfx_sprite_pack }
	trigger = {
		technology_group = void
		religion = the_watchers
	}
}

barbarian_company = {
	home_province = 993
	regiments_per_development = 0.05
	cavalry_weight = 0.25
	cavalry_cap = 4
	sprites = { westerngfx_sprite_pack }
	trigger = {
		primary_culture = barbarian
	}
}

frostguard_company = {
	home_province = 1075
	regiments_per_development = 0.05
	cavalry_weight = 0.25
	cavalry_cap = 4
	sprites = { westerngfx_sprite_pack }
	trigger = {
		OR = {
			tag = FRO
			was_tag = FRO
		}
	}
}

solari_company = {
	home_province = 605
    regiments_per_development = 0.1
	cavalry_weight = 0.25
	cavalry_cap = 4
	sprites = { westerngfx_sprite_pack }
    trigger = {
		primary_culture = solari
	}
	modifier = { 
		fire_damage_received = -0.10
		shock_damage_received = -0.10
	}
}

lunari_company = {
	home_province = 605
    regiments_per_development = 0.1
	cavalry_weight = 0.25
	cavalry_cap = 4
	sprites = { westerngfx_sprite_pack }
    trigger = {
		primary_culture = lunari
	}
	modifier = { 
		fire_damage = 0.10
		shock_damage = 0.10
	}
}