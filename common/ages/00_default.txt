end_of_the_darkin_wars = {
	start = 300
	can_start = { always = yes }
	objectives = {
		obj_100_development = {
			total_development = 100
		}
		
		obj_two_continents = {
			num_of_continents = 2
		}
		
		obj_trade = {
			OR = {
				trading_bonus = {
					trade_goods = god_essence
					value = yes
				}
				trading_bonus = {
					trade_goods = void_essence
					value = yes
				}
			}
		}
		
		obj_5_centers_of_trade = {
			calc_true_if = {
				all_owned_province = {
					OR = {
						province_has_center_of_trade_of_level = 2
						province_has_center_of_trade_of_level = 3
					}
					controlled_by = owner
					is_core = ROOT
				}
				amount = 5
			}
		}
		
		obj_30_development_city = {
			custom_trigger_tooltip = {
				tooltip = obj_30_development_city_tooltip
				any_owned_province = {
					is_core = ROOT
					controlled_by = owner
					development_discounting_tribal = 30
				}
			}
		}
		
		obj_many_vassals = {
			vassal = 4
		}
		
		obj_humiliate_rival = {
			custom_trigger_tooltip = {
				tooltip = obj_humiliate_rival_tooltip
				has_country_flag = humiliated_rival
			}
		}
	}
	abilities = {
		ab_allow_feudal_de_jure_law_1 = {
			effect = {
				custom_tooltip = feudal_de_jure_law
				set_country_flag = feudal_de_jure_law
			}
			ai_will_do = {
				factor = 10
			}
		}
		
		ab_justified_wars_1 = {
			modifier = {
				ae_impact = -0.1
			}
			ai_will_do = {
				factor = 10
			}			
		}

		ab_transfer_vassal_wargoal_1 = 
		{
			rule = {
				can_transfer_vassal_wargoal = yes
				can_chain_claim = yes
			}
			ai_will_do = {
				factor = 10
			}
		}
		
		ab_free_war_taxes_1 = {
			modifier = {
				war_taxes_cost_modifier = -1.0
			}
			ai_will_do = {
				factor = 10
			}	
		}
		
		ab_attack_bonus_in_capital_terrain_1 = {
			rule = {
				attack_bonus_in_capital_terrain = yes
			}
			ai_will_do = {
				factor = 10
			}
		}
		
		ab_avarosan_production_1 = {
			allow = {
				tag = AVA
			}
			modifier = {
				production_efficiency = 0.20
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_winters_claw_strength_1 = {
			allow = {
				tag = WNC
			}
			modifier = {
				cavalry_power = 0.10
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_frostguard_religion_1 = {
			allow = {
				tag = FRO
			}
			modifier = {
				missionaries = 1	
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_volibears_siege_abililty_1 = {
			allow = {
				tag = URS
			}
			modifier = {
				siege_ability = 0.10
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_anivias_defense_1 = {
			allow = {
				tag = ANI
			}
			modifier = {
				defensiveness = 0.15
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_ornns_durability_1 = {
			allow = {
				tag = ORN
			}
			modifier = {
				land_attrition = -0.20
			}
			ai_will_do = {
				factor = 100
			}
		}
	}
}

reign_of_the_iron_revenant = {
	start = 450
	can_start = {
		OR = {
			exists = NOX
			is_year = 450
		}
	}
	objectives = {
		obj_100_development = {
			total_development = 100
		}
		
		obj_two_continents = {
			num_of_continents = 2
		}
		
		obj_trade = {
			OR = {
				trading_bonus = {
					trade_goods = god_essence
					value = yes
				}
				trading_bonus = {
					trade_goods = void_essence
					value = yes
				}
			}
		}
		
		obj_5_centers_of_trade = {
			calc_true_if = {
				all_owned_province = {
					OR = {
						province_has_center_of_trade_of_level = 2
						province_has_center_of_trade_of_level = 3
					}
					controlled_by = owner
					is_core = ROOT
				}
				amount = 5
			}
		}
		
		obj_30_development_city = {
			custom_trigger_tooltip = {
				tooltip = obj_30_development_city_tooltip
				any_owned_province = {
					is_core = ROOT
					controlled_by = owner
					development_discounting_tribal = 30
				}
			}
		}
		
		obj_many_vassals = {
			vassal = 4
		}
		
		obj_humiliate_rival = {
			custom_trigger_tooltip = {
				tooltip = obj_humiliate_rival_tooltip
				has_country_flag = humiliated_rival
			}
		}
	}
	abilities = {
		ab_allow_feudal_de_jure_law_2 = {
			effect = {
				custom_tooltip = feudal_de_jure_law
				set_country_flag = feudal_de_jure_law
			}
			ai_will_do = {
				factor = 10
			}
		}
		
		ab_justified_wars_2 = {
			modifier = {
				ae_impact = -0.1
			}
			ai_will_do = {
				factor = 10
			}			
		}

		ab_transfer_vassal_wargoal_2 = 
		{
			rule = {
				can_transfer_vassal_wargoal = yes
				can_chain_claim = yes
			}
			ai_will_do = {
				factor = 10
			}
		}
		
		ab_free_war_taxes_2 = {
			modifier = {
				war_taxes_cost_modifier = -1.0
			}
			ai_will_do = {
				factor = 10
			}	
		}
		
		ab_attack_bonus_in_capital_terrain_2 = {
			rule = {
				attack_bonus_in_capital_terrain = yes
			}
			ai_will_do = {
				factor = 10
			}
		}
		
		ab_avarosan_production_2 = {
			allow = {
				tag = AVA
			}
			modifier = {
				production_efficiency = 0.20
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_winters_claw_strength_2 = {
			allow = {
				tag = WNC
			}
			modifier = {
				cavalry_power = 0.10
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_frostguard_religion_2 = {
			allow = {
				tag = FRO
			}
			modifier = {
				missionaries = 1	
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_volibears_siege_abililty_2 = {
			allow = {
				tag = URS
			}
			modifier = {
				siege_ability = 0.10
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_anivias_defense_2 = {
			allow = {
				tag = ANI
			}
			modifier = {
				defensiveness = 0.15
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_ornns_durability_2 = {
			allow = {
				tag = ORN
			}
			modifier = {
				land_attrition = -0.20
			}
			ai_will_do = {
				factor = 100
			}
		}
	}
}

the_ruination = {
	start = 600
	can_start = {
		OR = {
			exists = SHA
			is_year = 600
		}
	}
	objectives = {
		obj_100_development = {
			total_development = 100
		}
		
		obj_two_continents = {
			num_of_continents = 2
		}
		
		obj_trade = {
			OR = {
				trading_bonus = {
					trade_goods = god_essence
					value = yes
				}
				trading_bonus = {
					trade_goods = void_essence
					value = yes
				}
			}
		}
		
		obj_5_centers_of_trade = {
			calc_true_if = {
				all_owned_province = {
					OR = {
						province_has_center_of_trade_of_level = 2
						province_has_center_of_trade_of_level = 3
					}
					controlled_by = owner
					is_core = ROOT
				}
				amount = 5
			}
		}
		
		obj_30_development_city = {
			custom_trigger_tooltip = {
				tooltip = obj_30_development_city_tooltip
				any_owned_province = {
					is_core = ROOT
					controlled_by = owner
					development_discounting_tribal = 30
				}
			}
		}
		
		obj_many_vassals = {
			vassal = 4
		}
		
		obj_humiliate_rival = {
			custom_trigger_tooltip = {
				tooltip = obj_humiliate_rival_tooltip
				has_country_flag = humiliated_rival
			}
		}
	}
	abilities = {
		ab_allow_feudal_de_jure_law_3 = {
			effect = {
				custom_tooltip = feudal_de_jure_law
				set_country_flag = feudal_de_jure_law
			}
			ai_will_do = {
				factor = 10
			}
		}
		
		ab_justified_wars_3 = {
			modifier = {
				ae_impact = -0.1
			}
			ai_will_do = {
				factor = 10
			}			
		}

		ab_transfer_vassal_wargoal_3 = 
		{
			rule = {
				can_transfer_vassal_wargoal = yes
				can_chain_claim = yes
			}
			ai_will_do = {
				factor = 10
			}
		}
		
		ab_free_war_taxes_3 = {
			modifier = {
				war_taxes_cost_modifier = -1.0
			}
			ai_will_do = {
				factor = 10
			}	
		}
		
		ab_attack_bonus_in_capital_terrain_3 = {
			rule = {
				attack_bonus_in_capital_terrain = yes
			}
			ai_will_do = {
				factor = 10
			}
		}
		
		ab_avarosan_production_3 = {
			allow = {
				tag = AVA
			}
			modifier = {
				production_efficiency = 0.20
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_winters_claw_strength_3 = {
			allow = {
				tag = WNC
			}
			modifier = {
				cavalry_power = 0.10
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_frostguard_religion_3 = {
			allow = {
				tag = FRO
			}
			modifier = {
				missionaries = 1	
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_volibears_siege_abililty_3 = {
			allow = {
				tag = URS
			}
			modifier = {
				siege_ability = 0.10
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_anivias_defense_3 = {
			allow = {
				tag = ANI
			}
			modifier = {
				defensiveness = 0.15
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_ornns_durability_3 = {
			allow = {
				tag = ORN
			}
			modifier = {
				land_attrition = -0.20
			}
			ai_will_do = {
				factor = 100
			}
		}
	}
}

resurgence_of_civilizations = {
	start = 750
	can_start = {
		OR = {
			is_institution_enabled = resurgence_of_civilizations
			is_year = 750
		}
	}
	objectives = {
		obj_100_development = {
			total_development = 100
		}
		
		obj_two_continents = {
			num_of_continents = 2
		}
		
		obj_trade = {
			OR = {
				trading_bonus = {
					trade_goods = god_essence
					value = yes
				}
				trading_bonus = {
					trade_goods = void_essence
					value = yes
				}
			}
		}
		
		obj_5_centers_of_trade = {
			calc_true_if = {
				all_owned_province = {
					OR = {
						province_has_center_of_trade_of_level = 2
						province_has_center_of_trade_of_level = 3
					}
					controlled_by = owner
					is_core = ROOT
				}
				amount = 5
			}
		}
		
		obj_30_development_city = {
			custom_trigger_tooltip = {
				tooltip = obj_30_development_city_tooltip
				any_owned_province = {
					is_core = ROOT
					controlled_by = owner
					development_discounting_tribal = 30
				}
			}
		}
		
		obj_many_vassals = {
			vassal = 4
		}
		
		obj_humiliate_rival = {
			custom_trigger_tooltip = {
				tooltip = obj_humiliate_rival_tooltip
				has_country_flag = humiliated_rival
			}
		}
	}
	abilities = {
		ab_allow_feudal_de_jure_law_4 = {
			effect = {
				custom_tooltip = feudal_de_jure_law
				set_country_flag = feudal_de_jure_law
			}
			ai_will_do = {
				factor = 10
			}
		}
		
		ab_justified_wars_4 = {
			modifier = {
				ae_impact = -0.1
			}
			ai_will_do = {
				factor = 10
			}			
		}

		ab_transfer_vassal_wargoal_4 = 
		{
			rule = {
				can_transfer_vassal_wargoal = yes
				can_chain_claim = yes
			}
			ai_will_do = {
				factor = 10
			}
		}
		
		ab_free_war_taxes_4 = {
			modifier = {
				war_taxes_cost_modifier = -1.0
			}
			ai_will_do = {
				factor = 10
			}	
		}
		
		ab_attack_bonus_in_capital_terrain_4 = {
			rule = {
				attack_bonus_in_capital_terrain = yes
			}
			ai_will_do = {
				factor = 10
			}
		}
		
		ab_avarosan_production_4 = {
			allow = {
				tag = AVA
			}
			modifier = {
				production_efficiency = 0.20
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_winters_claw_strength_4 = {
			allow = {
				tag = WNC
			}
			modifier = {
				cavalry_power = 0.10
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_frostguard_religion_4 = {
			allow = {
				tag = FRO
			}
			modifier = {
				missionaries = 1	
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_volibears_siege_abililty_4 = {
			allow = {
				tag = URS
			}
			modifier = {
				siege_ability = 0.10
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_anivias_defense_4 = {
			allow = {
				tag = ANI
			}
			modifier = {
				defensiveness = 0.15
			}
			ai_will_do = {
				factor = 100
			}
		}
		
		ab_ornns_durability_4 = {
			allow = {
				tag = ORN
			}
			modifier = {
				land_attrition = -0.20
			}
			ai_will_do = {
				factor = 100
			}
		}
	}
}