government = republic
add_government_reform = pirate_republic_reform
government_rank = 1
mercantilism = 15
technology_group = shuriman
religion = eternals
primary_culture = kalamanda
capital = 1591

300.1.1 = {
	monarch = {
		name = "Lysimachus"
		adm = 2
		dip = 3
		mil = 2
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = eternals
		culture = kalamanda
		female = yes
	}
}