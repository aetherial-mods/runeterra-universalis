government = theocracy
government_rank = 2
mercantilism = 25
technology_group = shuriman
religion = solari_lunari
primary_culture = solari
capital = 605

historical_friend = SHU

historical_rival = MNS

300.1.1 = {
	monarch = {
		name = "Dahirios"
		dynasty = "Solaris"
		adm = 0
		dip = 1
		mil = 3
		birth_date = 281.1.1
		death_date = 341.1.1
		religion = solari_lunari
		culture = solari
	}
	heir = {
		name = "Toro"
		dynasty = "Solaris"
		adm = 0
		dip = 7
		mil = 0
		birth_date = 296.1.1
		death_date = 356.1.1
		religion = solari_lunari
		culture = solari
		claim = 100
	}
}