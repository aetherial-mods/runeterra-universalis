government = monarchy
government_rank = 1
mercantilism = 0
technology_group = ixtaly
religion = mother_serpent
primary_culture = buhru
capital = 3030

300.1.1 = {
	monarch = {
		name = "Harkahome"
		dynasty = "Huata"
		adm = 3
		dip = 3
		mil = 3
		birth_date = 280.1.1
		death_date = 340.1.1
		religion = mother_serpent
		culture = buhru
	}
	heir = {
		name = "Adahy"
		dynasty = "Huata"
		adm = 3
		dip = 4
		mil = 1
		birth_date = 280.1.1
		death_date = 340.1.1
		religion = mother_serpent
		culture = buhru
		claim = 100
	}
}