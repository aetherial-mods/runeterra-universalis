government = monarchy
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = ascended
capital = 1867

300.1.1 = {
	monarch = {
		name = "Resephi"
		dynasty = "Marammen"
		adm = 4
		dip = 2
		mil = 3
		birth_date = 268.1.1
		death_date = 328.1.1
		religion = eternals
		culture = ascended
	}
	heir = {
		name = "Apune"
		dynasty = "Marammen"
		adm = 5
		dip = 6
		mil = 2
		birth_date = 290.1.1
		death_date = 350.1.1
		religion = eternals
		culture = ascended
		claim = 100
	}
}