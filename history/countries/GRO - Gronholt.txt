government = monarchy
government_rank = 1
mercantilism = 0
technology_group = freljordian
religion = frostguards
primary_culture = frostguards
capital = 1133

300.1.1 = {
	monarch = {
		name = "Rula"
		dynasty = "Unnr"
		adm = 2
		dip = 4
		mil = 1
		birth_date = 276.1.1
		death_date = 336.1.1
		religion = frostguards
		culture = frostguards
	}
	heir = {
		name = "Latham"
		dynasty = "Unnr"
		adm = 3
		dip = 2
		mil = 1
		birth_date = 291.1.1
		death_date = 351.1.1
		religion = frostguards
		culture = frostguards
		claim = 100
	}
}