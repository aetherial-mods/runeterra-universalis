government = tribal
government_rank = 1
mercantilism = 0
technology_group = ixtaly
religion = axiomata
religious_school = water_school
primary_culture = buhru
capital = 735

300.1.1 = {
	monarch = {
		name = "Ohnya"
		dynasty = "Yolotli"
		adm = 1
		dip = 1
		mil = 5
		birth_date = 273.1.1
		death_date = 333.1.1
		religion = axiomata
		culture = buhru
	}
	
	heir = {
		name = "Yolonen"
		dynasty = "Yolotli"
		adm = 1
		dip = 5
		mil = 6
		birth_date = 289.1.1
		death_date = 349.1.1
		religion = axiomata
		culture = buhru
		claim = 100
	}
}