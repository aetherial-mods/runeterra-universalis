government = monarchy
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = nashramaen
capital = 1646

300.1.1 = {
	monarch = {
		name = "Madpara"
		dynasty = "Pheleoas"
		adm = 1
		dip = 6
		mil = 1
		birth_date = 266.1.1
		death_date = 326.1.1
		religion = eternals
		culture = nashramaen
	}
	heir = {
		name = "Rempi"
		dynasty = "Pheleoas"
		adm = 1
		dip = 2
		mil = 3
		birth_date = 281.1.1
		death_date = 341.1.1
		religion = eternals
		culture = nashramaen
		claim = 100
	}
}