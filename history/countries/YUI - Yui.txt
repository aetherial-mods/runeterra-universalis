government = tribal
government_rank = 1
mercantilism = 0
technology_group = ionian
religion = spirit_blossom
religious_school = nature_school
primary_culture = navorian
capital = 2543

300.1.1 = {
	monarch = {
		name = "Tora"
		dynasty = "Koneko"
		adm = 3
		dip = 5
		mil = 1
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = spirit_blossom
		culture = navorian
	}
	heir = {
		name = "Chisana"
		dynasty = "Koneko"
		adm = 2
		dip = 4
		mil = 3
		birth_date = 300.1.1
		death_date = 360.1.1
		religion = spirit_blossom
		culture = navorian
		claim = 100
	}
}