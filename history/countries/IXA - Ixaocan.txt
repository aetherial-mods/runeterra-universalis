government = monarchy
government_rank = 2
mercantilism = 0
technology_group = ixtaly
religion = axiomata
religious_school = magma_school
primary_culture = ixaocania
capital = 689

300.1.1 = {
	monarch = {
		name = "Zolin"
		dynasty = "Yunalai"
		adm = 1
		dip = 5
		mil = 1
		birth_date = 264.1.1
		death_date = 324.1.1
		religion = axiomata
		culture = ixaocania
	}
	add_ruler_personality = charismatic_negotiator_personality
	
	heir = {
		name = "Tenya"
		dynasty = "Yunalai"
		adm = 4
		dip = 5
		mil = 5
		birth_date = 280.1.1
		death_date = 340.1.1
		religion = axiomata
		culture = ixaocania
		claim = 100
	}
}