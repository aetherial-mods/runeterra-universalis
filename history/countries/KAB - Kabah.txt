government = monarchy
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = farajan
capital = 1671

300.1.1 = {
	monarch = {
		name = "Poniuvos"
		dynasty = "Barbae"
		adm = 2
		dip = 6
		mil = 6
		birth_date = 274.1.1
		death_date = 334.1.1
		religion = eternals
		culture = farajan
	}
	heir = {
		name = "Yakub"
		dynasty = "Barbae"
		adm = 3
		dip = 5
		mil = 3
		birth_date = 289.1.1
		death_date = 349.1.1
		religion = eternals
		culture = farajan
		claim = 100
	}
}