government = native
government_rank = 1
mercantilism = 0
technology_group = shuriman
religion = eternals
primary_culture = kalamanda
capital = 1656

300.1.1 = {
	monarch = {
		name = "Anpu"
		adm = 2
		dip = 2
		mil = 0
		birth_date = 285.1.1
		death_date = 345.1.1
		religion = eternals
		culture = kalamanda
	}
}