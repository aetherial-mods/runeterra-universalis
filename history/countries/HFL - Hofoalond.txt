government = monarchy
government_rank = 1
mercantilism = 0
technology_group = freljordian
religion = freljordian_pantheon
primary_culture = avarosan
capital = 878

300.1.1 = {
	monarch = {
		name = "Galbat"
		dynasty = "Pirkko"
		adm = 2
		dip = 6
		mil = 3
		birth_date = 268.1.1
		death_date = 328.1.1
		religion = freljordian_pantheon
		culture = avarosan
	}
	heir = {
		name = "Gunnar"
		dynasty = "Pirkko"
		adm = 3
		dip = 3
		mil = 3
		birth_date = 283.1.1
		death_date = 343.1.1
		religion = freljordian_pantheon
		culture = avarosan
		claim = 100
	}
}