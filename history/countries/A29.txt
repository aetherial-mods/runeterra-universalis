government = tribal
government_rank = 1
mercantilism = 0
technology_group = ionian
religion = spirit_blossom
religious_school = magma_school
primary_culture = navorian
capital = 2331

300.1.1 = {
	monarch = {
		name = "Test"
		dynasty = "Test"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 270.1.1
		death_date = 330.1.1
		religion = spirit_blossom
		culture = navorian
	}
	heir = {
		name = "Test"
		dynasty = "Test"
		adm = 2
		dip = 2
		mil = 2
		birth_date = 285.1.1
		death_date = 345.1.1
		religion = spirit_blossom
		culture = navorian
		claim = 100
	}
}