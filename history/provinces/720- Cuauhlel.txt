add_core = WIT
owner = WIT
controller = WIT
is_city = yes
culture = killash
religion = axiomata
capital = "Cuauhlel"
trade_goods = spices
hre = no
base_tax = 3
base_production = 3
base_manpower = 3

discovered_by = shuriman
discovered_by = ixtaly