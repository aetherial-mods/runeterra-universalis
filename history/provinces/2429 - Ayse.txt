add_core = PHY
owner = PHY
controller = PHY
culture = navorian
religion = spirit_blossom
capital = "Ayse"
trade_goods = fish
hre = no
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes

discovered_by = ionian