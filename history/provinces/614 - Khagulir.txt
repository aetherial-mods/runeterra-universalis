add_core = TAR
owner = TAR
controller = TAR
culture = solari
religion = solari_lunari
capital = "Dern Badur"
trade_goods = gems
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = shuriman