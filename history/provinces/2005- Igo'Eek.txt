add_core = OGM
owner = OGM
controller = OGM
culture = watcher
religion = the_watchers
capital = "Igo'Eek"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = void