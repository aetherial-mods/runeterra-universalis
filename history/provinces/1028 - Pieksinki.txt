add_core = DAM
owner = DAM
controller = DAM
culture = avarosan
religion = freljordian_pantheon
capital = "Savonkaupunki"
trade_goods = fur
hre = no
is_city = yes
base_tax = 1
base_production = 1
base_manpower = 1
center_of_trade = 1

discovered_by = freljordian