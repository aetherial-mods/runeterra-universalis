add_core = ONM
owner = ONM
controller = ONM
culture = navorian
religion = spirit_blossom
capital = "Luna"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = ionian