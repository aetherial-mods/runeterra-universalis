add_core = KAT
owner = KAT
controller = KAT
culture = wuju
religion = spirit_blossom
capital = "Bani"
trade_goods = grain
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = ionian