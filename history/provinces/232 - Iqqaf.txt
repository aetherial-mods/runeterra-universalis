add_core = IQQ
owner = IQQ
controller = IQQ
culture = ascended
religion = eternals
capital = "Iqqaf"
trade_goods = iron
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = shuriman
discovered_by = void