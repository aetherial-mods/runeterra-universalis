add_core = SHD
owner = SHD
controller = SHD
culture = ascended
religion = eternals
capital = "Maerid"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = shuriman
discovered_by = void