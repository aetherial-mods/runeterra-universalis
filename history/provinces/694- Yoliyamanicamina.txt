add_core = CHA
owner = CHA
controller = CHA
is_city = yes
culture = ixaocania
religion = axiomata
capital = "Yoliyamanicamina"
trade_goods = iron
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = shuriman
discovered_by = isles
discovered_by = ixtaly