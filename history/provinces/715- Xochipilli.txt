add_core = IXA
owner = IXA
controller = IXA
is_city = yes
culture = ixaocania
religion = axiomata
capital = "Xochipilli"
trade_goods = spices
hre = no
base_tax = 2
base_production = 2
base_manpower = 2

discovered_by = shuriman
discovered_by = isles
discovered_by = ixtaly