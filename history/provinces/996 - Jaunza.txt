add_core = WNC
owner = WNC
controller = WNC
culture = barbarian
religion = freljordian_pantheon
capital = "Kulbe"
trade_goods = fish
hre = no
is_city = yes
base_tax = 3
base_production = 3
base_manpower = 3

discovered_by = ursines
discovered_by = icewall

discovered_by = freljordian