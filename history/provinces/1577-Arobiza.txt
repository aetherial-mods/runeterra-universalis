add_core = NER
owner = NER
controller = NER
culture = farajan
religion = eternals
capital = "Mareya"
trade_goods = camels
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = shuriman