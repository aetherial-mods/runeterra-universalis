add_core = LYS
owner = LYS
controller = LYS
culture = frostguards
religion = frostguards
capital = "Hólnes"
trade_goods = black_iron
hre = no
is_city = yes
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = freljordian