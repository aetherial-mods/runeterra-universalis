add_core = JAC
owner = JAC
controller = JAC
is_city = yes
culture = killash
religion = axiomata
capital = "Zipacyotli"
trade_goods = spices
hre = no
base_tax = 3
base_production = 3
base_manpower = 3

discovered_by = shuriman
discovered_by = ixtaly