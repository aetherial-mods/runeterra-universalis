add_core = MHJ
owner = MHJ
controller = MHJ
culture = ascended
religion = eternals
capital = "Sawt 'ajash"
trade_goods = spices
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = shuriman
discovered_by = void
discovered_by = ixtaly