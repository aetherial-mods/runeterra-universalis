add_core = TLE
owner = TLE
controller = TLE
is_city = yes
culture = buhru
religion = mother_serpent
capital = "Citlalhuitl"
trade_goods = fish
hre = no
base_tax = 3
base_production = 3
base_manpower = 3

discovered_by = isles
discovered_by = ixtaly