add_core = B01
owner = B01
controller = B01
culture = watcher
religion = the_watchers
capital = "Bur'Dam"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = void