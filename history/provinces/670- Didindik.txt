add_core = MES
owner = MES
controller = MES
culture = crawler
religion = the_watchers
capital = "Didindik"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = shuriman
discovered_by = void
discovered_by = ixtaly