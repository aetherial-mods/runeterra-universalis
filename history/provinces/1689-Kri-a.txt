add_core = NAS
owner = NAS
controller = NAS
culture = nashramaen
religion = eternals
capital = "Kri-a"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = shuriman