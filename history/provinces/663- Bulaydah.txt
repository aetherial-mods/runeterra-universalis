add_core = AKT
owner = AKT
controller = AKT
culture = devourer
religion = the_watchers
capital = "Bulaydah"
trade_goods = fish
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = shuriman
discovered_by = void