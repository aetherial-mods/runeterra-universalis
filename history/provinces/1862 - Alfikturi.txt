add_core = HUD
owner = HUD
controller = HUD
culture = ascended
religion = the_watchers
capital = "Alfikturi"
trade_goods = spices
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = shuriman
discovered_by = void
discovered_by = ixtaly