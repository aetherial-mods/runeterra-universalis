add_core = SHI
owner = SHI
controller = SHI
culture = kalamanda
religion = eternals
capital = "Hakor"
trade_goods = slaves
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = shuriman