add_core = YRQ
owner = YRQ
controller = YRQ
culture = farajan
religion = eternals
capital = "Jarga"
trade_goods = camels
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = shuriman