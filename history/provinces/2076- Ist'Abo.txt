add_core = ELI
owner = ELI
controller = ELI
culture = watcher
religion = the_watchers
capital = "Ist'Abo"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = void