add_core = SHU
owner = SHU
controller = SHU
culture = ascended
religion = eternals
capital = "Sanalah"
trade_goods = camels
hre = no
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes

discovered_by = shuriman