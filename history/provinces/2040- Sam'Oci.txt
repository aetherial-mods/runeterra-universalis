add_core = EVR
owner = EVR
controller = EVR
culture = watcher
religion = the_watchers
capital = "Sam'Oci"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = void