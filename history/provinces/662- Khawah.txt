add_core = DYT
owner = DYT
controller = DYT
culture = crawler
religion = the_watchers
capital = "Khawah"
trade_goods = fish
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = shuriman
discovered_by = void
discovered_by = isles
discovered_by = ixtaly