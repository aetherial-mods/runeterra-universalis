add_core = FRO
owner = FRO
controller = FRO
culture = frostguards
religion = frostguards
capital = "Nartu"
trade_goods = cloth
hre = no
is_city = yes
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = freljordian