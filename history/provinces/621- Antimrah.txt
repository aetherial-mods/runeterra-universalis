add_core = ICR
owner = ICR
controller = ICR
culture = icathian
religion = eternals
capital = "Antimrah"
trade_goods = fish
hre = no
base_tax = 5
base_production = 5
base_manpower = 5

discovered_by = shuriman
discovered_by = void
discovered_by = ixtaly