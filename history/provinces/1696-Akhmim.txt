add_core = ZUR
owner = ZUR
controller = ZUR
culture = nashramaen
religion = eternals
capital = "Akhmim"
trade_goods = camels
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = shuriman