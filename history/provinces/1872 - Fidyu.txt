add_core = XSR
owner = XSR
controller = XSR
culture = crawler
religion = the_watchers
capital = "Fidyu"
trade_goods = camels
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = shuriman
discovered_by = void
discovered_by = ixtaly