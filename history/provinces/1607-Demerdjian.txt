add_core = ARD
owner = ARD
controller = ARD
culture = kalamanda
religion = eternals
capital = "Irmine"
trade_goods = copper
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = shuriman