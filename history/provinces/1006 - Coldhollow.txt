add_core = FRH
owner = FRH
controller = FRH
culture = avarosan
religion = freljordian_pantheon
capital = "Winterdale"
trade_goods = fish
hre = no
is_city = yes
base_tax = 3
base_production = 3
base_manpower = 3

discovered_by = freljordian