add_core = EMI
owner = EMI
controller = EMI
culture = hiranan
religion = spirit_blossom
capital = "Tomino"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = ionian