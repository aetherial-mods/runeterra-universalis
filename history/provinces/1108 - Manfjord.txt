add_core = FRO
owner = FRO
controller = FRO
culture = frostguards
religion = frostguards
capital = "Haugerum"
trade_goods = fur
hre = no
is_city = yes
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = freljordian