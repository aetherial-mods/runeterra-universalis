add_core = ALS
owner = ALS
controller = ALS
culture = kalamanda
religion = eternals
capital = "Shati"
trade_goods = fish
hre = no
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes

discovered_by = shuriman