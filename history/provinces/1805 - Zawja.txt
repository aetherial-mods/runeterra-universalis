add_core = WIT
owner = WIT
controller = WIT
is_city = yes
culture = killash
religion = eternals
capital = "Zawja"
trade_goods = camels
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = shuriman
discovered_by = void
discovered_by = ixtaly