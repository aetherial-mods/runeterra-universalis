add_core = TRK
owner = TRK
controller = TRK
culture = avarosan
religion = freljordian_pantheon
capital = "Nyshamn"
trade_goods = fur
hre = no
is_city = yes
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = freljordian