add_core = QUC
owner = QUC
controller = QUC
culture = barbarian
religion = freljordian_pantheon
capital = "Quchar"
trade_goods = ivory
hre = no
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes

discovered_by = ursines
discovered_by = icewall

discovered_by = freljordian