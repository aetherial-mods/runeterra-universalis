add_core = YUI
owner = YUI
controller = YUI
culture = navorian
religion = spirit_blossom
capital = "Volaa"
trade_goods = iron
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = ionian