add_core = KAN
owner = KAN
controller = KAN
is_city = yes
culture = buhru
religion = axiomata
capital = "Coamac"
trade_goods = fish
hre = no
base_tax = 3
base_production = 3
base_manpower = 3

discovered_by = isles
discovered_by = ixtaly