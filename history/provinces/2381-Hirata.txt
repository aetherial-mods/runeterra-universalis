add_core = ZHY
owner = ZHY
controller = ZHY
culture = zhyun
religion = spirit_blossom
capital = "Hirata"
trade_goods = iron
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = ionian