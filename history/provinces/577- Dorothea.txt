add_core = HEL
owner = HEL
controller = HEL
is_city = yes
culture = helian
religion = the_white_mist
capital = "Dorothea"
trade_goods = paper
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = isles