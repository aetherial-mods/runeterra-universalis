add_core = MNS
owner = MNS
controller = MNS
culture = ascended
religion = eternals
capital = "Aleahil"
trade_goods = camels
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = shuriman
discovered_by = void
discovered_by = ixtaly