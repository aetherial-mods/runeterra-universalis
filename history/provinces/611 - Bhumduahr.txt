add_core = TAR
owner = TAR
controller = TAR
culture = lunari
religion = solari_lunari
capital = "Virrigh"
trade_goods = gems
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = shuriman