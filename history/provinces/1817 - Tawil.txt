add_core = SAG
owner = SAG
controller = SAG
culture = bedouin
religion = eternals
capital = "Tawil"
trade_goods = camels
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = shuriman