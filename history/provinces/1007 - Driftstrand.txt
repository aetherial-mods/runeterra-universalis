add_core = FRH
owner = FRH
controller = FRH
culture = avarosan
religion = freljordian_pantheon
capital = "Frost"
trade_goods = petricit
hre = no
is_city = yes
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = freljordian