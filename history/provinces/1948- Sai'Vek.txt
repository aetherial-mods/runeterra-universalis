add_core = SKL
owner = SKL
controller = SKL
culture = watcher
religion = the_watchers
capital = "Sai'Vek"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = void