add_core = PHY
owner = PHY
controller = PHY
culture = navorian
religion = spirit_blossom
capital = "Pyla"
trade_goods = iron
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = ionian