add_core = BRE
owner = BRE
controller = BRE
culture = barbarian
religion = freljordian_pantheon
capital = "Vindhøj"
trade_goods = fish
hre = no
is_city = yes
base_tax = 2
base_production = 2
base_manpower = 2

discovered_by = ursines
discovered_by = icewall

discovered_by = freljordian