culture = barbarian
religion = freljordian_pantheon
capital = "Hattila"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 50
native_ferocity = 4
native_hostileness = 9


discovered_by = ursines
discovered_by = icewall
discovered_by = freljordian