add_core = BOA
owner = BOA
controller = BOA
culture = kinkou
religion = spirit_blossom
capital = "Jiyu"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = ionian