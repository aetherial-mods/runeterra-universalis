add_core = ANA
owner = ANA
controller = ANA
culture = crawler
religion = the_watchers
capital = "Jirorahan"
trade_goods = spices
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = shuriman
discovered_by = void
discovered_by = ixtaly