add_core = KLV
owner = KLV
controller = KLV
culture = frostguards
religion = frostguards
capital = "Rjudal"
trade_goods = cloth
hre = no
is_city = yes
base_tax = 3
base_production = 3
base_manpower = 3

discovered_by = freljordian