add_core = TCL
owner = TCL
controller = TCL
culture = troll
religion = freljordian_pantheon
capital = "Kunsa"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes


discovered_by = ursines
discovered_by = icewall

discovered_by = freljordian