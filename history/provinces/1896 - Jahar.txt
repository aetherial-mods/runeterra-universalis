add_core = MNS
owner = MNS
controller = MNS
culture = ascended
religion = eternals
capital = "Jahar"
trade_goods = camels
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = shuriman
discovered_by = void
discovered_by = ixtaly