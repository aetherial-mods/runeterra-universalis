add_core = MOS
owner = MOS
controller = MOS
culture = kalamanda
religion = eternals
capital = "Lobulege"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = shuriman