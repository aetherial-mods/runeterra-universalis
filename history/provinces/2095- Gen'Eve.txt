add_core = STV
owner = STV
controller = STV
culture = watcher
religion = the_watchers
capital = "Gen'Eve"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = void