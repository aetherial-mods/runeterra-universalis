add_core = ORN
owner = ORN
controller = ORN
culture = barbarian
religion = freljordian_pantheon
capital = "Kuhtila"
trade_goods = fish
hre = no
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes
center_of_trade = 2

discovered_by = ursines
discovered_by = icewall

discovered_by = freljordian