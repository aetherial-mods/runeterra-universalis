add_core = BND
owner = BND
controller = BND
culture = navorian
religion = spirit_blossom
capital = "Tchuqle"
trade_goods = wine
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = ionian