add_core = ANA
owner = ANA
controller = ANA
culture = crawler
religion = the_watchers
capital = "Jereirah"
trade_goods = void_essence
hre = no
base_tax = 3
base_production = 3
base_manpower = 3

add_cardinal = yes

discovered_by = shuriman
discovered_by = void
discovered_by = ixtaly