add_core = KAL
owner = KAL
controller = KAL
culture = kalamanda
religion = eternals
capital = "Mandonophis"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = shuriman