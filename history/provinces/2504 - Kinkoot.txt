add_core = KIN
owner = KIN
controller = KIN
culture = kinkou
religion = spirit_blossom
capital = "Kinkoot"
trade_goods = sugar
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = ionian