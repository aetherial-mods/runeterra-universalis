add_core = KAL
owner = KAL
controller = KAL
culture = kalamanda
religion = eternals
capital = "Horirem"
trade_goods = slaves
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = shuriman