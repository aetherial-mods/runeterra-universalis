add_core = NAV
owner = NAV
controller = NAV
culture = galrin
religion = spirit_blossom
capital = "Kuseko"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = ionian