add_core = FIR
owner = FIR
controller = FIR
culture = nashramaen
religion = eternals
capital = "Briybaha"
trade_goods = fish
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = shuriman