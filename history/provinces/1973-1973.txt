add_core = GAH
owner = GAH
controller = GAH
culture = watcher
religion = the_watchers
capital = "Arguin"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = void