add_core = JAF
owner = JAF
controller = JAF
culture = farajan
religion = eternals
capital = "Li-milra"
trade_goods = camels
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = shuriman