add_core = HFL
owner = HFL
controller = HFL
culture = avarosan
religion = freljordian_pantheon
capital = "Antru"
trade_goods = wool
hre = no
is_city = yes
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = freljordian