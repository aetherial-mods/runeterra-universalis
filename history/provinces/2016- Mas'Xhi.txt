add_core = KNS
owner = KNS
controller = KNS
culture = watcher
religion = the_watchers
capital = "Mas'Xhi"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = void