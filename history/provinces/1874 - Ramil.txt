add_core = TAL
owner = TAL
controller = TAL
culture = ascended
religion = eternals
capital = "Ramil"
trade_goods = camels
hre = no
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes

discovered_by = shuriman
discovered_by = void
discovered_by = ixtaly