add_core = KNC
owner = KNC
controller = KNC
culture = watcher
religion = the_watchers
capital = "Lat'Sou"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = void