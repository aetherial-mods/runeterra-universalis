add_core = VER
owner = VER
controller = VER
culture = avarosan
religion = frostguards
capital = "Kavarnge"
trade_goods = cloth
hre = no
is_city = yes
base_tax = 3
base_production = 3
base_manpower = 3


discovered_by = freljordian