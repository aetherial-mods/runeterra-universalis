add_core = FRG
owner = FRG
controller = FRG
culture = avarosan
religion = freljordian_pantheon
capital = "Frostbarrow"
trade_goods = fur
hre = no
is_city = yes
base_tax = 3
base_production = 3
base_manpower = 3

discovered_by = freljordian