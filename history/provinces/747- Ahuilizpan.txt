add_core = KAN
owner = KAN
controller = KAN
is_city = yes
culture = buhru
religion = mother_serpent
capital = "Ahuilizpan"
trade_goods = fish
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = isles
discovered_by = ixtaly