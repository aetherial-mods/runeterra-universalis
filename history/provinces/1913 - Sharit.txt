add_core = SUR
owner = SUR
controller = SUR
culture = farajan
religion = eternals
capital = "Sharit"
trade_goods = camels
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = shuriman