add_core = WNC
add_core = ICE
owner = ICE
controller = ICE
culture = barbarian
religion = freljordian_pantheon
capital = "Village Of The Ice Children"
trade_goods = iron
hre = no
is_city = yes
base_tax = 3
base_production = 3
base_manpower = 3


discovered_by = ursines
discovered_by = icewall

discovered_by = freljordian