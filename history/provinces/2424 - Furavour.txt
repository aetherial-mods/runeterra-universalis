add_core = QAE
owner = QAE
controller = QAE
culture = navorian
religion = spirit_blossom
capital = "Furavour"
trade_goods = iron
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = ionian