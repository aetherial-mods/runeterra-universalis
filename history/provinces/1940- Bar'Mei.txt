add_core = PMK
owner = PMK
controller = PMK
culture = watcher
religion = the_watchers
capital = "Bar'Mei"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = void