add_core = AZM
owner = AZM
controller = AZM
culture = hiranan
religion = spirit_blossom
capital = "Fuyuyasumi"
trade_goods = iron
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = ionian