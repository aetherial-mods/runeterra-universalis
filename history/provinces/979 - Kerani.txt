add_core = TRH
owner = TRH
controller = TRH
culture = troll
religion = freljordian_pantheon
capital = "Kousaari"
trade_goods = gold
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes

discovered_by = ursines
discovered_by = icewall

discovered_by = freljordian