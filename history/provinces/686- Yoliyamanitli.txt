add_core = BAT
owner = BAT
controller = BAT
is_city = yes
culture = ixaocania
religion = axiomata
capital = "Yoliyamanitli"
trade_goods = fish
hre = no
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = shuriman
discovered_by = isles
discovered_by = ixtaly