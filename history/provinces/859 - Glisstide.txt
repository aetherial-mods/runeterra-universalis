add_core = AVA
owner = AVA
controller = AVA
culture = avarosan
religion = freljordian_pantheon
capital = "Cristalwatch"
trade_goods = iron
hre = no
is_city = yes
base_tax = 1
base_production = 1
base_manpower = 1

discovered_by = freljordian