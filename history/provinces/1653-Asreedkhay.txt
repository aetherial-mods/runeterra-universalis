add_core = FIR
owner = FIR
controller = FIR
culture = nashramaen
religion = eternals
capital = "Asreedkhay"
trade_goods = slaves
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = shuriman