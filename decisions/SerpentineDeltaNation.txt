country_decisions = {

	serpentine_delta_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = formed_serpentine_delta_flag }
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			NOT = { tag = SER }
			capital_scope = {
				OR = {
					area = huitzilihuitl_area
					area = nochehuatl_area
					area = cuicacoehua_area
					area = camapichtli_area
				}
			}
		}
		provinces_to_highlight = {
			OR = {
				area = huitzilihuitl_area
				area = nochehuatl_area
				area = cuicacoehua_area
				area = camapichtli_area
			}
			OR = {
				NOT = { owned_by = ROOT }
				NOT = { is_core = ROOT }
			}
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			NOT = { exists = SER }
			huitzilihuitl_area = {
				type = all
				owned_by = ROOT
			}
			nochehuatl_area = {
				type = all
				owned_by = ROOT
			}
			cuicacoehua_area = {
				type = all
				owned_by = ROOT
			}
			camapichtli_area = {
				type = all
				owned_by = ROOT
			}
		}
		effect = {
			change_tag = SER
			add_prestige = 25
			serpentine_delta_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = SER
			}
			if = {
				limit = {
					NOT = { has_idea_group = SER_ideas }
				}
				country_event = { id = ideagroups.1 }
			}
			set_country_flag = formed_serpentine_delta_flag
		}
		ai_will_do = {
			factor = 1	
		}
	}
}