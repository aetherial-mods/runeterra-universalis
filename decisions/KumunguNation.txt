country_decisions = {

	kumungu_nation = {
		major = yes
		potential = {
			NOT = { has_country_flag = formed_kumungu_flag }
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			NOT = { tag = KUM }
			OR = {
				capital_scope = {
					region = kumungu_region	
				}
				AND = {
					capital_scope = {
						superregion = ixtal_superregion
					}
					government = native
				}
			}
		}
		provinces_to_highlight = {
			OR = {
				region = kumungu_region		
				province_id = 790
			}
			OR = {
				NOT = { owned_by = ROOT }
				NOT = { is_core = ROOT }
			}
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			NOT = { exists = KUM }
			government = tribal
			owns_core_province = 790
			num_of_owned_provinces_with = {
				is_core = ROOT
				region = kumungu_region
				value = 20
			}
		}
		effect = {
			change_tag = KUM
			set_government_rank = 2
			kumungu_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = KUM
			}
			if = {
				limit = {
					NOT = { has_idea_group = KUM_ideas }
				}
				country_event = { id = ideagroups.1 }
			}
			set_country_flag = formed_kumungu_flag
		}
		ai_will_do = {
			factor = 1
		}
	}
}