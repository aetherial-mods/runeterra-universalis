namespace = ru_transfer_nerimazeth_events
country_event = {
	id = ru_transfer_nerimazeth_events.1
	title = ru_transfer_nerimazeth_events.1.t
	desc = ru_transfer_nerimazeth_events.1.d
	picture = ru_nerimazeth_eventPicture

	is_triggered_only = yes
	
	option = {
		name = ru_transfer_nerimazeth_events.1.a
		highlight = yes
		trigger = {
			NER = {
				vassal_of = SHU
			}
			1575 = {
				owned_by = NER
			}
		}
		
		TAR = {
			vassalize = NER
			country_event = {
				id = ru_transfer_nerimazeth_events.2
				tooltip = transfer_nerimazeth_option_1_tooltip
			}
		}
		
		ai_chance = {
			modifier = {
				factor = 1
				NOT = { is_year = 400 }
			}
			modifier = {
				factor = 0
				is_year = 400
			}
		}
	}
	option = {
		name = ru_transfer_nerimazeth_events.1.b
		highlight = yes
		trigger = {
			OR = {
				AND = {
					1570 = {
						owned_by = NER
					}
					1575 = {
						owned_by = NER
					}
				}
				AND = {
					1570 = {
						owned_by = SHU
					}
					1575 = {
						owned_by = SHU
					}
				}
			}
		}
		if = {
			limit = {
				1570 = {
					owned_by = NER
				}
				1575 = {
					owned_by = NER
				}
			}
			1570 = {
				cede_province = TAR
				add_core = TAR
				remove_claim = TAR
				remove_core = NER
				remove_claim = NER
			}
			1575 = {
				cede_province = TAR
				add_core = TAR
				remove_claim = TAR
				remove_core = NER
				remove_claim = NER
			}
		}
		if = {
			limit = {
				1570 = {
					owned_by = SHU
				}
				1575 = {
					owned_by = SHU
				}
			}
			1570 = {
				cede_province = TAR
				add_core = TAR
				remove_claim = TAR
				remove_core = SHU
				remove_claim = SHU
			}
			1575 = {
				cede_province = TAR
				add_core = TAR
				remove_claim = TAR
				remove_core = SHU
				remove_claim = SHU
			}
		}
		TAR = {
			country_event = {
				id = ru_transfer_nerimazeth_events.3
				tooltip = transfer_nerimazeth_option_2_tooltip
			}
		}
		
		ai_chance = {
			factor = 0
		}
	}
	option = {
		name = ru_transfer_nerimazeth_events.1.c
		
		TAR = {
			country_event = {
				id = ru_transfer_nerimazeth_events.4
				tooltip = transfer_nerimazeth_option_3_tooltip
			}
			add_opinion = {
				who = SHU
				modifier = transfer_denied_opinion_mod
			}
		}
		
		ai_chance = {
			modifier = {
				factor = 1
				is_year = 400
			}
			modifier = {
				factor = 0
				NOT = { is_year = 400 }
			}
		}
	}
}
country_event = {
	id = ru_transfer_nerimazeth_events.2
	title = ru_transfer_nerimazeth_events.2.t
	desc = ru_transfer_nerimazeth_events.2.d
	picture = ru_nerimazeth_eventPicture

	is_triggered_only = yes
	
	option = {
		name = ru_transfer_nerimazeth_events.2.a
		
		ai_chance = {
			factor = 1
		}
	}
}
country_event = {
	id = ru_transfer_nerimazeth_events.3
	title = ru_transfer_nerimazeth_events.3.t
	desc = ru_transfer_nerimazeth_events.3.d
	picture = ru_nerimazeth_eventPicture

	is_triggered_only = yes
	
	option = {
		name = ru_transfer_nerimazeth_events.3.a
		
		ai_chance = {
			factor = 1
		}
	}
}
country_event = {
	id = ru_transfer_nerimazeth_events.4
	title = ru_transfer_nerimazeth_events.4.t
	desc = ru_transfer_nerimazeth_events.4.d
	picture = ru_nerimazeth_eventPicture

	is_triggered_only = yes
	
	option = {
		name = ru_transfer_nerimazeth_events.4.a
		
		add_country_modifier = {
			name = transfer_denied_mod
			duration = 9125
		}
		add_claim = 1575
		
		ai_chance = {
			factor = 1
		}
	}
}