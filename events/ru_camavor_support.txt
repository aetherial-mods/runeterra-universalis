namespace = ru_camavor_support
country_event = {
	id = ru_camavor_support.1
	title = ru_camavor_support.1.t
	desc = ru_camavor_support.1.d
	picture = ru_camavor_support_eventPicture

	is_triggered_only = yes
	
	trigger = {
		AND = {
			tag = CAM
			is_free_or_tributary_trigger = yes
		}
	}
	
	option = {
		name = ru_camavor_support.1.a
		highlight = yes
		
		hidden_effect = {
			country_event = {
				id = ru_camavor_support.1
				days = 1825
			}
			add_country_flag = cam_start_date_300_flag
		}
		if = {
			limit = {
				OR = {
					NOT = { treasury = 100 }
					num_of_loans = 1
				}
			}
			add_treasury = 100
		}
		if = {
			limit = {
				NOT = { manpower_percentage = 0.25 }
			}
			add_manpower = 5
		}
		if = {
			limit = {
				NOT = { sailors_percentage = 0.25 }
			}
			add_sailors = 500
		}
		if = {
			limit = {
				NOT = { adm_power = 0 }
			}
			add_adm_power = 100
		}
		if = {
			limit = {
				NOT = { dip_power = 0 }
			}
			add_dip_power = 100
		}
		if = {
			limit = {
				NOT = { mil_power = 0 }
			}
			add_mil_power = 100
		}
		if = {
			limit = {
				NOT = { army_size = 1 }
			}
			random_owned_province = {
				limit = { has_port = yes }
				ROOT = {
					infantry = PREV
					infantry = PREV
					infantry = PREV
					infantry = PREV
					infantry = PREV
				}
			}
		}
		if = {
			limit = {
				NOT = { navy_size = 1 }
			}
			random_owned_province = {
				limit = { has_port = yes }
				ROOT = {
					light_ship = PREV
					light_ship = PREV
					light_ship = PREV
					light_ship = PREV
					light_ship = PREV
				}
			}
		}
		
		ai_chance = {
			factor = 1
		}
	}
}