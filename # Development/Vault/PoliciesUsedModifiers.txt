##############
ADMINISTRATIVE
##############

Centralised + Religous = global_missionary_strength = 2
Centralised + Humanist = country_admin_power = 1
Centralised + Expansion = colonist_placement_chance = 0.05
Centralised + Exploration = expel_minorities_cost = -0.25
Centralised + Espionage = prestige_decay = -0.01
Centralised + Subject = advisor_pool = 2
Centralised + Privateer = adm_tech_cost_modifier = -0.05
Centralised + Caravan = great_project_upgrade_cost = -0.25
Centralised + Navy = core_creation = -0.1
Centralised + Armada = possible_policy = 1
Centralised + Shock = reform_progress_growth = 0.1
Centralised + Fire = administrative_efficiency = 0.025
Centralised + Offensive = innovativeness_gain = 0.1
Centralised + Defensive = monarch_lifespan = 0.25

Decentralised + Religous = missionaries = 1
Decentralised + Humanist = legitimacy republican_tradition devotion horde_unity meritocracy
Decentralised + Expansion = state_maintenance_modifier = -0.1
Decentralised + Exploration = global_institution_spread = 0.1
Decentralised + Espionage = state_governing_cost = -0.1
Decentralised + Subject = advisor_cost = -0.1
Decentralised + Caravan = inflation_reduction = -0.1
Decentralised + Armada = global_ship_recruit_speed = -0.1
Decentralised + Shock = development_cost = -0.1
Decentralised + Fire = production_efficiency = 0.1
Decentralised + Quantity = build_cost = -0.1
Decentralised + Quality = build_time = -0.1
Decentralised + Defensive = yearly_corruption = -0.05

Religious + Expansion = tolerance_heretic = 1
Religious + Exploration = tolerance_heathen = 1
Religious + Espionage = missionary_maintenance_cost = -0.1
Religious + Subject = tolerance_own = 1
Religious + Fire = global_tax_modifier = 0.1
Religious + Defensive = stability_cost_modifier = -0.1

Humanist + Expansion = native_uprising_chance = -0.5
Humanist + Exploration = range = 0.25
Humanist + Espionage = years_of_nationalism = -5
Humanist + Caravan = idea_cost = -0.1
Humanist + Quantity = religious_unity = 15
Humanist + Quality = embracement_cost = -0.1
Humanist + Offensive = prestige_from_land = 0.25
Humanist + Defensive = global_unrest = -2

Expansion + Espionage = ae_impact = -0.1
Expansion + Subject = colonists = 1
Expansion + Caravan = global_supply_limit_modifier = 0.1
Expansion + Navy = governing_capacity_modifier = 0.1
Expansion + Quantity = native_assimilation = 0.5
Expansion + Quality = global_colonial_growth = 25



##########
DIPLOMATIC
##########

Decentralised + Privateer = disengagement_chance = 0.1
Decentralised + Navy = global_naval_barrage_cost = -0.1

Religious + Privateer = culture_conversion_cost = -0.1
Religious + Navy = heavy_ship_cost = -0.1
Religious + Armada = galley_cost = -0.1

Humanist + Subject = diplomatic_reputation = 1
Humanist + Privateer = num_accepted_cultures = 1
Humanist + Navy = movement_speed_in_fleet_modifier = 0.1
Humanist + Armada = light_ship_cost = -0.1

Expansion + Armada = global_tariffs = 0.25
Expansion + Shock = war_exhaustion_cost = -0.25
Expansion + Fire = war_exhaustion = -0.1

Exploration + Subject = diplomatic_upkeep = 1
Exploration + Privateer = navy_tradition_decay = -0.1
Exploration + Caravan = own_coast_naval_combat_bonus = 1
Exploration + Navy = naval_tradition_from_battle = 0.25
Exploration + Armada = treasure_fleet_income = 0.25
Exploration + Quality = transport_power = 0.1

Espionage + Privateer = rival_change_cost = -0.5
Espionage + Navy = dip_tech_cost_modifier = -0.05
Espionage + Armada = transport_cost = -0.1
Espionage + Shock = fabricate_claims_cost = -0.1
Espionage + Quality = rebel_support_efficiency = 0.1
Espionage + Offensive = spy_offence = 0.1
Espionage + Defensive = global_spy_defence = 0.1

Subject + Privateer = diplomatic_annexation_cost = -0.1
Subject + Caravan = liberty_desire_from_subject_development = -0.25
Subject + Navy = envoy_travel_time = -0.1
Subject + Armada = heir_chance = 0.25
Subject + Shock = reduced_liberty_desire = 10
Subject + Quantity = improve_relation_modifier = 0.2
Subject + Quality = country_diplomatic_power = 1
Subject + Offensive = vassal_income = 0.1
Subject + Defensive = diplomats = 1

Privateer + Navy = light_ship_power = 0.1
Privateer + Armada = global_trade_power = 0.1
Privateer + Shock = trade_range_modifier = 0.25
Privateer + Fire = war_taxes_cost_modifier = -0.1
Privateer + Quantity = global_prov_trade_power_modifier = 0.1
Privateer + Quality = privateer_efficiency = 0.1
Privateer + Offensive = global_ship_trade_power = 0.1
Privateer + Defensive = embargo_efficiency = 0.1

Caravan + Navy = trade_steering = 0.1
Caravan + Armada = naval_forcelimit_modifier = 0.15
Caravan + Shock = trade_efficiency = 0.1
Caravan + Fire = global_trade_goods_size_modifier = 0.1
Caravan + Quantity = merchants = 1
Caravan + Quality = caravan_power = 0.1
Caravan + Defensive = mercantilism_cost = -0.1

Navy + Shock = prestige_from_naval = 0.25
Navy + Fire = naval_morale = 0.1
Navy + Quantity = naval_maintenance_modifier = -0.1
Navy + Quality = ship_durability = 0.1
Navy + Offensive = heavy_ship_power = 0.1
Navy + Defensive = global_ship_repair = 0.1

Armada + Shock = allowed_marine_fraction = 0.25
Armada + Fire = sunk_ship_morale_hit_recieved = -0.1
Armada + Quantity = global_ship_cost = -0.1
Armada + Quality = recover_navy_morale_speed = 0.1
Armada + Offensive = galley_power = 0.1
Armada + Defensive = naval_attrition = -0.1



########
MILITARY
########

Centralised + Quantity = global_manpower_modifier = 0.1
Centralised + Quality = country_military_power = 1

Decentralised + Offensive = manpower_recovery_speed = 0.1

Religious + Caravan = loot_amount = 0.25
Religious + Shock = drill_gain_modifier = 0.25
Religious + Quantity = global_regiment_recruit_speed = -0.1
Religious + Quality = drill_decay_modifier = -0.25
Religious + Offensive = cavalry_power = 0.1

Humanist + Shock = merc_maintenance_modifier = -0.1
Humanist + Fire = recover_army_morale_speed = 0.1

Expansion + Privateer = land_maintenance_modifier = -0.1
Expansion + Offensive = mercenary_discipline = 0.05
Expansion + Defensive = mercenary_cost = -0.1

Exploration + Espionage = reinforce_speed = 0.1
Exploration + Shock = army_tradition_decay = -0.1
Exploration + Fire = army_tradition_from_battle = 0.25
Exploration + Quantity = global_regiment_cost = -0.1
Exploration + Offensive = land_attrition = -2
Exploration + Defensive = hostile_attrition = 2

Espionage + Caravan = available_province_loot = 0.25
Espionage + Fire = province_warscore_cost = -0.1
Espionage + Quantity = mil_tech_cost_modifier = -0.05

Subject + Fire = siege_ability = 0.1

Caravan + Offensive = movement_speed = 0.1

Shock + Quantity = free_leader_pool = 1
Shock + Quality = shock_damage = 0.1
Shock + Offensive = infantry_power = 0.1
Shock + Defensive = shock_damage_received = -0.1

Fire + Quantity = leader_cost = -0.1
Fire + Quality = fire_damage = 0.1
Fire + Offensive = artillery_power = 0.1
Fire + Defensive = fire_damage_received = -0.1

Quantity + Offensive = land_morale = 0.1
Quantity + Defensive = defensiveness = 0.1

Quality + Offensive = discipline = 0.05
Quality + Defensive = fort_maintenance_modifier = -0.1