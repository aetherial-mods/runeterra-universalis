Hello Guys!
Today I worked out the Nation of Targon a little bit.
Just some Ideas floating around my head.

So the Nation of Targon will be formable later in the game.
The area starts with the Nation of "Rakkor" will be the Ruler over all Shuriman Nations in the Form of Daimyos like Ashikaga.

And like Ashikaga the Nations Rakkan, Targon or whoever is the "Shogun" at the time will have a similiar Government Type.
Where you got 3 different Buttons with different Effects.
But unlike Ashikaga you can use all 3 at all times without Cooldown.

Here is the twist though:

Because in the Lore Targon got control over Aurelion Sol because of the Golden Crown, I wanted to use this system.
At some point when Aurelion gets his powers back he will eventually break free from Targons chains and get his revenge on the Mountain.

And with that every time you use one of the 3 actions provided by the Government Type a certain Gouge will fill up.
When the Gouge reaches 100% Aurelion will break free and every Province in the "Targon" Region will get 50 Devestation together with 10 Unrest and the Disbandment of the Targon Daimoy System.
So every Shuriman Nation is now on its own not protected by the Shogun anymore.

The Gouge will not go down over time and will stay at the Percentage it is. Only Events will change it sometimes.

So here are the different Options you can pick whenever you want:



Option 1:

Name: Artificial Sun
Aurelion Cost: 5
Effect: Increase Production of the Province "Targon" by 3.



Option 2:

Name: Appoint an Ascended.
Aurelion Cost: 5
Effect: Get a General with 100 Tradition.



Option 3:

Name: Keep them under Control
Aurelion Cost: 10
Effect: Reduce Liberty Desire of all Subjects by 10 percent.



Option 4 (Event):

Name: Voids Invasion
Aurelion Cost: 100
Effect: Close the Void Portal in Targon.



The last one (Option 4) becomes available as soon as the Portal to the Void opens in Targon and the Shogun gets a one time chance only to close the portal at the cost of setting Aurelion Sol free and losing the Government Form.