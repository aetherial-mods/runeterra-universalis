##################################################################
### Terrain Categories
###
### Terrain types: plains, mountains, hills, desert, artic, forest, jungle, marsh, pti
### Types are used by the game to apply certain bonuses/maluses on movement/combat etc.
###
### Sound types: plains, forest, desert, sea, jungle, mountains

categories =  {
	pti = {
		type = pti
	}

	ocean = {
		color = { 255 255 255 }

		sound_type = sea
		is_water = yes

		movement_cost = 1.0
		
		terrain_override = {
			42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
			61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80
			81 82 83 84 85 86 87 88 89 90 91 92 93 94
			98 99 100
			101 102 103 104 105
			128 129	130 131 132 133
			136 137 138 139 140
			141 142 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 160
			161 162 163 164 165 166 167 168 169 170 171 172 173 174 175 176 177 178 179 180
			181 182 183 184 185 186 187 188 189 190 191 192 193 194 195 196 197 198 199 200
			201 202 203 204
			207 208 209 210 211 212 213 214 215 216
			219 220
			246 247 248 249 250 251 252 253 254 255 256 257 258 259 260
			261 262 263 264 265 266 267 268 269 270 271 272 273 274 275 276 277 278 279 280
			281 282 283 284 285 286 287 288 289 290 291 292 293 294 295 296 297 298 299 300
			301 302 303 304 305 306 307 308 309 310 311 312 313 314 315 316 317 318 319 320
			321 322 323 324 325 326 327 328 329 330 331 332 333 334 335 336 337 338 339 340
			341 342 343 344 345 346 347 348 349 350 351 352 353 354 355 356 357 358 359 360
			361 362 363 364 365 366 367 368 369 370 371 372 373 374 375 376 377 378 379 380
			381 382 383 384 385 386 387 388 389 390 391 392 393 394 395 396 397 398 399 400
			401 402 403 404 405 406 407 408 409 410 411 412 413 414 415 416 417 418 419 420
			421 422 423 424 425 426 427 428 429 430 431 432 433 434 435 436 437 438 439 440
			441 442 443 444 445 446 447 448 449 450 451 452 453 454 455 456 457 458 459 460
			461 462 463 464 465
		}
	}

	inland_ocean = {
		color = { 0 0 200 }

		sound_type = sea
		is_water = yes
		inland_sea = yes

		movement_cost = 1.0
		
		terrain_override = {
			2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
			31 32 33 34 35 36 37 38 39 40 41
			95 96 97
			106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122
			123 124 125 126 127
			134 135
			230
			684
			3023 3024
			3036
		}
	}	

	glacier = {
		color = { 235 235 235 }

		type = plains
		sound_type = desert

		defence = 1
		movement_cost = 1.25
		supply_limit = 2		
		local_development_cost = 0.5
		nation_designer_cost_multiplier = 0.75		
		
		terrain_override = {		
			238
			241
			466 467 468 469 470 471 472 473 474 475 476 477 478 479 480
			481 482 483 484 485 486 487 488 489 490 491 492 493 494 495 496 497 498 499 500
			501 502 503 504 505 506 507 508 509 510 511 512 513 514 515 516 517 518 519 520
			521 522 523 524 525 526 527 528 529 530 531 532 533 534 535 536 537 538 539 540
			541 542 543 544
			690
			875
			919
			922
			928
			931 932 933 934 935 936 937 938 939
			947
			949
			970
			971 972 973 974 975 976 977 978 979 980 981 982 983 984 985 986 987 988
			992 993 994 995 996 997 998 999 1000
			1045
			1058 1059 1060
			1061 1062 1063 1064
			1067
			1069 1070
			1071 1072 1073 1074
			1077 1078 1079 1080
			1081 1082 1083 1084	1085 1086 1087 1088 1089 1090 1091 1092 1093 1094 1095 1096 1097 1098 1099
			1102 1103
			1104 1105 1106 1107 1108 1109 1110
			1111 1112 1113 1114 1115 1116 1117 1118 1119 1120
			1121 1122 1123 1124 1125 1126 1127 1128 1129
			1133 1134 1135 1136 1137 1138 1139 1140
			1141 1142 1143 1144 1145 1146 1147 1148 1149 1150
			1151 1152 1153 1154 1155 1156 1157 1158 1159
		}
	}

	farmlands = {
		color = { 179 255 64 }

		type = plains
		sound_type = plains

		movement_cost = 1.10
		local_development_cost = -0.05
		supply_limit = 10
		allowed_num_of_buildings = 1
		nation_designer_cost_multiplier = 1.05
		
		terrain_override = {
			2395
			2397 2398 2399
			2401 2402 2403 2404 2405 2406 2407 2408 2409 2410
			2452
			2466 2467 2468
			2471
			2478
			2480
			2481
			2484
			2487 2488 2489 2490 2491 2492 2493 2494 2495 2496 2497 2498 2499
		}		
	}
	
	forest = {
		color = { 18 74 9 }

		type = forest
		sound_type = forest
		
		movement_cost = 1.25
		defence = 1
		supply_limit = 4
		local_development_cost = 0.2
		nation_designer_cost_multiplier = 0.9
		
		terrain_override = {
			549 550 551 552
			830
			839 840 841
			852 853
			855 856 857 858 859 860 861 862
			865
			867 868
			870
			882 883 884 885 886 889 890 891 892 893 894 895 896 897 898
			900 901 902
			904 905 906
			908 909 910
			911 912 913 914 915 916 917 918
			920
			921 923 924 925 926 927
			929 930
			1001 1002 1003 1004 1005 1006 1007 1008 1009 1010
			1011 1012 1013 1014 1015 1016
			1017 1018 1019
			1020
			1021 1022 1023 1024 1025
			1026 1027 1028 1029 1030
			1031 1032 1033 1034
			1035 1036 1037 1038 1039 1040
			1041 1042 1043 1044 
			1046 1047 1048 1049	1050
			1051 1052 1053 1054 1055 1056 1057
			1065 1066
			1068
			1100 1101
			1130 1131 1132
			2500
			2501 2502 2503 2504 2505 2506 2507 2508 2509
			2511 2512 2513 2514 2515 2516 2517 2518 2519 2520
			2521
			2523 2524 2525 2526 2527 2528 2529
			3025
		}                                      
	}
	
	hills = {
		color = { 113 176 151 }

		type = hills
		sound_type = mountains

		movement_cost = 1.40
		defence = 1
		local_defensiveness = 0.1
		local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.85
		supply_limit = 5
		
		terrain_override = {
			234
			239
			545 546 547 548
			587 588
			741
			744
			746
			749
			780
			803
			811
			814
			820 821
			824
			968 969
			2351
			2387 2388
			2396
			2400
			2438
			2440
			2441 2442 2443 2444 2445 2446 2447 2448 2449 2450
			2455
			2459
			2463
			2465
			2482
			2719 2720
			2721
		}
	}

	
	woods = {
		color = { 41 155 22 }

		type = forest
		sound_type = forest

		movement_cost = 1.10
		defence = 1
		local_development_cost = 0.15
		nation_designer_cost_multiplier = 0.9
		supply_limit = 6
		
		terrain_override = {
		}
	}
	
	mountain = {
		color = { 105 24 4 }

		type = mountains
		sound_type = mountains

		movement_cost = 1.5
		defence = 2
		local_defensiveness = 0.25
		local_development_cost = 0.35
		nation_designer_cost_multiplier = 0.75
		supply_limit = 4
		
		terrain_override = {
			232
			240
			556
			569 570
			791 792
			794
			795
			828
			872 873
			1473
			1480
			1482 1483 1484
			1487 1488 1489 1490
			1491 1492
			1495 1496
			1508 1509 1510
			1513 1514
			1517
			1527
			1530
			1533
			1537
			1540
			1543 1544
			1546
			1548 1549
			1571
			1603
			1611 1614 1615 1616
			1626 1627 1628
			1630
			1631
			1634
			1844 1845 1846
			1868 1869 1870
			1878 1879
			1889
			1895
			1902 1903
			1914 1915 1916
			1928 1930
			1931 1932 1933 1934 1935 1936 1937
			2337
			2343 2344
			2357
			2360
			2361
			2366 2367
			2370 2371 2372
			2374 2375 2376 2377
			2381
			2383
			2419
			2424
			2426 2427
			2429
			2431
			2472 2473
			2483
			2485 2486
			2510
			2522
			2532
			2535 2536
			2539
			2541
			2548
			2550
			2554
		}
	}

	grasslands = {
		color = { 90 235 27 }

		type = plains
		sound_type = plains

		movement_cost = 1.0
		supply_limit = 8
		allowed_num_of_buildings = 1
		nation_designer_cost_multiplier = 1
		
		terrain_override = {
			553 554
			561 562 563 564 565 566 567 568
			571 572 573 574 575 576 577
			694
			707 708 709
			711
			714 715
			723
			727
			733
			767 768
			781 782 783 784 785 786 787
			789 790
			793
			829
			831 832 833 834 835 836 837 838
			842 843 844 845 846 847 848 849 850	851
			863 864
			866
			869
			871
			874
			876 877 878 879 880 881
			940 941 942 943 944 945 946
			948
			950 951 952 953 954 955 956 957 958 959 960
			961 962 963 964 965 966 967
			1466
			1468
			1526
			1529 
			1532
			1536
			1541
			1547
			1597
			1600
			1606 1607 1608
			1610
			1636
			1741
			1755
			1757
			1760
			1840
			1841 1842 1843
			1867
			1899 1900
			1901
			2378 2379 2380
			2382
			2384 2385 2386
			2389 2390 2391
			2474
			2530 2531
			2533 2534
			2537 2538
			2540
			2542 2543 2544 2545 2546 2547
			2549
			2551 2552 2553
			2555 2556 2557 2558 2559 2560
			2561 2562 2563 2564 2565 2566 2567 2568 2569 2570 2571 2572 2573 2574 2575
			3021
		}
	}

	jungle = {
		color = { 98 163 18 }

		type = jungle
		sound_type = jungle

		movement_cost = 1.5
		defence = 1
		local_development_cost = 0.35
		nation_designer_cost_multiplier = 0.8
		supply_limit = 5
		
		terrain_override = {
			231
			710
			716
			718 719 720
			721 722
			724 725 726
			728 729 730
			731 732
			734
			737 738 739 740
			742 743
			745
			750
			751 752
			755
			757 758 759
			765 766
			769 770
			788
			796 797 798 799 800
			801 802
			804 805 806
			808 809 810
			812 813
			815 816 817 818 819
			822 823
			825 826 827
			3026
		}
	}	
	
	marsh = {
		color = { 13 189 130 }

		type = marsh
		sound_type = forest

		movement_cost = 1.3
		defence = 1
		local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.85
		supply_limit = 5

		terrain_override = {
			581
			585 586
			675 676 677 678 679
			747 748
			753 754
			760
			761 762 763
			854
			899
			1999
			2729
			3027 3028 3029 3030
			3031 3032 3033 3034 3035	
		}
	}
	
	desert = {
		color = { 242 242 111 }

		type = desert
		sound_type = desert

		movement_cost = 1.05
		supply_limit = 4
		local_development_cost = 0.50
		nation_designer_cost_multiplier = 0.8
		
		terrain_override = {
			1474
			1479
			1481
			1485 1486
			1493
			1497 1498 1499
			1511
			1516
			1535
			1538
			1545
			1550
			1552
			1560
			1561
			1564 1565 1566 1567 1568 1569 1570
			1573 1574 1575 1576 1577
			1580
			1582 1583 1584 1585 1586 1587 1588
			1597
			1600
			1606 1607 1608
			1610
			1617 1618 1619 1620
			1621 1622 1623 1624 1625
			1633
			1637 1638 1639 1640
			1641
			1643 1644
			1646
			1648
			1652 1653 1654 1655 1656 1657 1658 1659 1660
			1661 1662 1663 1664 1665 1666
			1672
			1674 1675 1676 1677 1678
			1680
			1681 1682 1683 1684 1685 1686
			1691 1692 1693 1694 1695 1696 1697 1698 1699 1700
			1701 1702 1703 1704 1705
			1711 1712
			1716
			1718
			1720
			1721 1722 1723
			1726
			1731 1732 1733 1734
			1737 1738 1739
			1743
			1747 1748
			1761 1762 1763 1764 1765 1766 1767 1768 1769 1770
			1771 1772 1773 1774 1775 1776 1777 1778 1779 1780
			1781 1782 1783 1784 1785 1786 1787 1788 1789 1790
			1791 1792 1793 1794 1795
			1797
			1800
			1802 1803 1804 1805 1806
			1808
			1810
			1811 1812 1813 1814 1815 1816 1817 1818
			1820
			1821 1822 1823 1824 1825 1826 1827
			1830
			1848 1849 1850
			1851 1852 1853 1854 1855 1856 1857 1858 1859
			1872 1873 1874 1875 1876
			1880
			1881 1882 1883 1884 1885 1886 1887 1888
			1890
			1891 1892 1893 1894
			1896 1897 1898
			1904 1905 1906 1907
			1917 1918 1919 1920
			1921 1922 1923 1924 1925 1926 1927
			1929 1930
		}
	}
	
	coastal_desert = {
		color = { 255 211 110 }

		type = desert
		sound_type = desert

		movement_cost = 1.0
		local_development_cost = 0.35
		nation_designer_cost_multiplier = 0.9		
		supply_limit = 4

		terrain_override = {
		}	
	}
	
	coastline = {
		color = { 49 175 191 }

		sound_type = sea

		movement_cost = 1.0
		local_development_cost = 0.15
		nation_designer_cost_multiplier = 0.85		
		supply_limit = 6 
		
		terrain_override = {
			233
			555
			557 558 559 560
			578 579 580
			582 583 584
			606 607 608 609 610
			613 614 616 617 618 619 620
			621 622 623
			625
			627 628 629 630
			631 632
			634 635
			644
			646
			648 649 650
			661 662 663 664
			680
			681 682 683
			685 686 687 688
			691 692	693
			695 696 697 698 699 700
			701 702
			712 713
			717
			735 736
			772 773 774 775 776 777 778
			756
			764
			887 888
			903
			907
			1461 1462 1463 1464 1465
			1467
			1469 1470
			1471 1472
			1475
			1478
			1494
			1500
			1501 1502 1503 1504 1505 1506 1507
			1512
			1525
			1528
			1531
			1572
			1591 1592 1593 1594 1595 1596
			1598 1599
			1601 1602
			1609
			1632
			1635
			1642
			1645 
			1647
			1649 1650
			1651
			1687 1688 1689 1690
			1735 1736
			1740
			1742
			1744 1745 1746
			1749 1750
			1751 1752 1753 1754
			1756
			1758 1759
			1801
			1831 1832 1833 1834 1835 1836 1837 1838 1839
			2326 2327 2328 2329 2330 2331 2332 2333 2334 2335 2336
			2338 2339 2340 
			2341 2342
			2345 2346 2347 2348 2349 2350
			2352 2353 2354 2355 2356
			2358 2359
			2362 2363 2364 2365
			2368 2369
			2373
			2393 2394
			2411 2412 2413 2414 2415 2416 2417 2418
			2420
			2421 2422 2423
			2425
			2428
			2430
			2432 2433 2434 2435 2436
			2439
			2453 2454
			2456 2457
			2461 2462
			2464
			2475 2476 2477
			2479
			2710
			2711 2712 2713 2714
			2716 2717 2718
			2722 2723 2724 2725 2726 2727 2728 
		}
	}	
	
	drylands = {
		color = { 232 172 102 }		

		type = plains
		sound_type = plains

		movement_cost = 1.00
		local_development_cost = 0.05
		nation_designer_cost_multiplier = 0.95
		supply_limit = 7
		allowed_num_of_buildings = 1

		terrain_override = {
		}
	}

	highlands = {
		color = { 176 129 21 }

		type = hills
		sound_type = mountains
		
		supply_limit = 6
		movement_cost = 1.40
		defence = 1
		local_defensiveness = 0.1
		local_development_cost = 0.2
		nation_designer_cost_multiplier = 0.9	

		terrain_override = {
		}
	}

	savannah = {
		color = { 248 199 23  }

		sound_type = plains

		supply_limit = 6
		movement_cost = 1.00
		local_development_cost = 0.15
		nation_designer_cost_multiplier = 0.95	
		
		terrain_override = {
			1453 1454 1455 1456 1457 1458 1459 1460
			1515
			1518 1519 1520
			1521 1522 1523 1524
			1553 1554 1556 1557 1558 1559
			1562 1563
			1578 1579
			1581
			1589 1590
			1667 1668 1669 1670
			1671
			1673
			1679
			1706 1707 1708 1709 1710
			1728 1729 1730
			1796
			1798 1799
			1819
			1847
			1908 1909 1910
			1911 1912 1913 
		}		
	}
	
	steppe = {
		color = { 147 200 83  }

		type = plains
		sound_type = plains

		movement_cost = 1.00
		local_development_cost = 0.20
		nation_designer_cost_multiplier = 0.9	
		supply_limit = 6
		
		terrain_override = {
			665 666
			668 669
			1807
			1809
			1828 1829
			1860
			1861 1862 1863 1864 1865 1866
			1871
		}	
	}
	
	impassable_mountains = {
		color = { 128 128 128 }

		type = mountains
		sound_type = mountains

		movement_cost = 2.0
		defence = 3
		local_development_cost = 1.0

		terrain_override = {
			611 612
			615
		}		
	}
	
	void_touched = {
		color = { 90 0 190 }
		
		type = mountains
		sound_type = mountains
		
		movement_cost = 1.25
		defence = 1
		local_development_cost = 0.25
		local_build_time = 0.25
		
		terrain_override = {
			245
			624
			633
			637 638 639 640
			641 642 643
			645
			647
			651 652 653 654 655 656 657 658 659 660
			667
			670
			671 672 673 674
			1877
		}
	}
	
	void = {
		color = { 30 30 30 }
		
		type = mountains
		sound_type = mountains
		
		local_development_cost = 20
		supply_limit = 45
		
		terrain_override = {
			242 243
			1940
			1941 1942 1943 1944 1945 1946 1947 1948 1949 1950
			1951 1952 1953 1954 1955 1956 1957 1958 1959 1960
			1961 1962 1963 1964 1965 1966 1967 1968 1969 1970
			1971 1972 1973 1974 1975 1976 1977 1978 1979 1980
			1981 1982 1983 1984 1985 1986 1987 1988 1989 1990
			1991 1992 1993 1994 1995 1996 1997 1998
			2000
			2001 2002 2003 2004 2005 2006 2007 2008 2009 2010
			2011 2012 2013 2014 2015 2016 2017 2018 2019 2020
			2021 2022 2023 2024 2025 2026 2027 2028 2029 2030
			2031 2032 2033 2034 2035 2036 2037 2038 2039 2040
			2041 2042 2043 2044 2045 2046 2047 2048 2049 2050
			2051 2052 2053 2054 2055 2056 2057 2058 2059 2060
			2061 2062 2063 2064 2065 2066 2067 2068 2069 2070
			2071 2072 2073 2074 2075 2076 2077 2078 2079 2080
			2081 2082 2083 2084 2085 2086 2087 2088 2089 2090
			2091 2092 2093 2094 2095 2096 2097 2098 2099 2100
			2101 2102 2103 2104 2105 2106 2107 2108 2109 2110
			2111 2112
			3022
		}
	}
	
	lava = {
		color = { 255 0 0 }
		
		type = mountains
		sound_type = mountains
		
		movement_cost = 1.5
		local_development_cost = 0.35
		defence = 1
		local_defensiveness = 0.20
		
		terrain_override = {
			989 991
		}
	}
	
	volcano = {
		color = { 155 0 0 }
		
		type = mountains
		sound_type = mountains
		
		movement_cost = 1.5
		local_development_cost = 2
		defence = 3
		allowed_num_of_buildings = -7
		attrition = 5
		
		terrain_override = {
			990
		}
	}
	
	strait = {
		color = { 255 190 91 }
		
		type = mountains
		sound_type = mountains
		
		movement_cost = 1.5
		local_development_cost = 0.35
		defence = 1
		local_defensiveness = 0.20
		supply_limit = 3
		
		terrain_override = {
			771
			779
			1534
			1542
			1551
			1555
			1713 1714 1715
			1717
		}
	}
	
	underground = {
		color = { 167 94 125 }
		
		type = mountains
		sound_type = mountains
		
		movement_cost = 1.25
		local_development_cost = 0.25
		defence = 1
		local_defensiveness = 0.1
		supply_limit = 5
		
		terrain_override = {
			206
			807
			1605
			1613
			1629
		}
	}
	
	frostguard_citadel = {
        color = { 33 56 138 }
        
        type = plains
		sound_type = plains
		
		supply_limit = 10
		movement_cost = 1
		defence = 2
		local_defensiveness = 0.25

		terrain_override = {
			1075 1076
		}
    }
	
	the_sun_disc = {
        color = { 255 130 0 }
        
        type = plains
		sound_type = plains
		
		movement_cost = 1
		supply_limit = 10
		local_development_cost = -0.15
		local_build_time = -0.15
		local_defensiveness = 0.25

		terrain_override = {
			1449 1450 1451 1452
		}
    }
	
	mount_targon = {
        color = { 130 0 255 }
        
        type = plains
		sound_type = plains
		
		supply_limit = 10
		movement_cost = 2
		defence = 5
		local_defensiveness = 0.50

		terrain_override = {
			605
		}
    }
	
	ixaocan = {
        color = { 90 255 180 }
        
        type = plains
		sound_type = plains
		
		supply_limit = 10
		movement_cost = 1
		defence = 1
		local_institution_spread = 0.25
		local_unrest = -2

		terrain_override = {
			689
			703 704 705 706
		}
    }
	
	navori_placidium = {
        color = { 255 0 120 }
        
        type = plains
		sound_type = plains
		
		supply_limit = 10
		movement_cost = 1
		defence = 1
		local_institution_spread = 1
		local_development_cost = -0.1
		allowed_num_of_buildings = 1

		terrain_override = {
			2470
		}
    }
}
	
##################################################################
### Graphical terrain
###		type	=	refers to the terrain defined above, "terrain category"'s 
### 	color 	= 	index in bitmap color table (see terrain.bmp)
###

terrain = {
	grasslands			= { type = grasslands		color = { 	0	 } }
	hills				= { type = hills			color = { 	1	 } }
	desert_mountain		= { type = mountain			color = { 	2	 } }
	desert				= { type = desert			color = { 	3	 } }

	plains				= { type = grasslands		color = { 	4	 } }
	terrain_5			= { type = grasslands		color = { 	5	 } }
	mountain			= { type = mountain			color = { 	6	 } }
	desert_mountain_low	= { type = desert			color = { 	7	 } }

	terrain_8			= { type = hills			color = { 	8	 } }
	marsh				= { type = marsh			color = { 	9	 } }
	terrain_10			= { type = farmlands		color = { 	10	 } }
	terrain_11			= { type = farmlands		color = { 	11	 } }

	forest_12			= { type = forest			color = { 	12	 } }
	forest_13			= { type = forest			color = { 	13	 } }
	forest_14			= { type = forest			color = { 	14	 } }
	ocean				= { type = ocean			color = { 	15	 } }

	snow				= { type = mountain 		color = { 	16	 } } # (SPECIAL CASE) Used to identify permanent snow
	inland_ocean_17 	= { type = inland_ocean		color = {	17	 } }

	coastal_desert_18	= { type = coastal_desert	color = { 	19	 } }
	coastline			= { type = coastline		color = { 	35	 } }
	
	savannah			= { type = savannah 		color = {	20	 } }
	drylands			= { type = drylands			color = {	22	 } }
	highlands			= { type = highlands		color = {	23	 } }
	dry_highlands		= { type = highlands		color = {	24	 } }
	
	woods				= { type = woods			color = { 	255	 } }
	jungle				= { type = jungle			color = { 	254	 } }
	
	terrain_21			= { type = farmlands		color = { 	21	 } }	
}

##################################################################
### Tree terrain
###		terrain	=	refers to the terrain tag defined above
### 	color 	= 	index in bitmap color table (see tree.bmp)
###

tree = {
	forest				= { terrain = forest 			color = { 	3 4 6 7 19 20	} }
	woods				= { terrain = woods 			color = { 	2 5 8 18	} }
	jungle				= { terrain = jungle 			color = { 	13 14 15	} }
	palms				= { terrain = desert 			color = { 	12	} }
	savana				= { terrain = grasslands 		color = { 	27 28 29 30	} }
}